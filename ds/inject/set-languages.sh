#!/bin/bash -x

source /host/settings.sh
languages=${1:-$LANGUAGES}

### set the list of languages for import
sed -i /var/www/data/config.sh \
    -e "/^languages=/c languages=\"$languages\""

### update drupal configuration
drush @local_btr --yes vset btr_languages "$languages"
drush @local_btr --yes php-eval "module_load_include('inc', 'btrCore', 'admin/core'); btrCore_config_set_languages();"

### copy btr_languages to the translations database
drush @btr sql-query --database=btr_data "
    DROP TABLE IF EXISTS btr_languages;
    CREATE TABLE btr_languages SELECT * FROM $DBNAME.btr_languages;
"

### add these languages to drupal and import their translations
for lng in $languages; do
    drush @local_btr --yes language-add $lng
done
if [[ -z $DEV ]]; then
    drush @local_btr --yes l10n-update-refresh
    drush @local_btr --yes l10n-update
fi
