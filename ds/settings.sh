APP=btr_server/ds
DOMAIN="btr.example.org"
DOMAINS="dev.$DOMAIN"
IMAGE=$DOMAIN

### Uncomment if this installation is for development.
DEV=true

### Admin settings.
ADMIN_PASS=123456
ADMIN_EMAIL=admin@example.org

### Translation languages supported by the B-Translator Server.
### Do not remove 'fr', because sometimes French translations
### are used instead of template files (when they are missing).
LANGUAGES='fr de it sq'

### DB settings
DBHOST=mariadb
DBPORT=3306
DBNAME=${DOMAIN//./_}
DBUSER=${DOMAIN//./_}
DBPASS=${DOMAIN//./_}
