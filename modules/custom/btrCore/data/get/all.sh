#!/bin/bash

### go to this directory
cd $(dirname $0)

./gnome.sh        # needs to be fixed manually
./kde.sh
./firefox-os.sh
./office.sh       # needs to be fixed manually
./mozilla.sh
./ubuntu.sh       # needs to be fixed manually
#./drupal.sh       # install languages on Drupal first
#./wordpress.sh   # deprecated
#./misc.sh        # deprecated
#./mandriva.sh    # deprecated
#./fedora.sh      # deprecated
#./xfce.sh        # deprecated

