#!/bin/bash
### Save the schema of the database btr_data.

source /host/settings.sh

### go to the script directory
cd $(dirname $0)

### get the list of the tables
mysql=$(drush @btr sql-connect --database=btr_data)
tables=$($mysql -B -e "SHOW TABLES" | grep '^btr_' )

### dump only the schema of the database
database=$(echo $mysql | tr ' ' "\n" | grep database= | cut -d= -f2)
sql_connect=$(echo $mysql | sed -e 's/^mysql //' -e 's/--database=[^ ]*//')
mysqldump $sql_connect \
          --column-statistics=0 --no-data --compact --add-drop-table \
          $database $tables > btr_schema.sql

### fix a little bit the file
sed -e '/^SET /d' -i btr_schema.sql

echo "
Schema saved to:
  $(pwd)/btr_schema.sql
"
